FROM ubuntu:16.04

RUN apt-get -qq -y update && \
    apt-get -qq -y install wget && \
    echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt testing jdk1.8" | tee /etc/apt/sources.list.d/bazel.list && \
    wget -O - https://storage.googleapis.com/bazel-apt/doc/apt-key.pub.gpg | apt-key add - && \
    apt-get -qq -y update && \
    apt-get -qq -y install bazel
